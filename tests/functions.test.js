
const calculate_area_rectangle = require('../src/functions');

test('simple formula', () => {
    expect(calculate_area_rectangle(2,3)).toBe(6);
});



[
    {length: 1, long: -2},
    {length: -1, long: 1},
    {length: -2, long: -1}
].forEach( values => {
    test(`.callNegative($values.length, $values.long)`, () => {
        expect(
            () => {
                calculate_area_rectangle(values.length, values.long);
            }
        ).toThrow(RangeError);     
      });
} ); 
