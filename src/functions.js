function calculate_area_rectangle(length, long) {
    if (typeof length !== 'number' || typeof long !== 'number') {
        throw new TypeError('Unexpected type');
    }
    if (length < 0 || long < 0) {
        throw new RangeError('Negative value unexpected');
    }
    return length * long;
}

module.exports = calculate_area_rectangle;